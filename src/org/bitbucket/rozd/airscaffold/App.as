/**
 * Created by max.rozdobudko@gmail.com on 2/8/17.
 */
package org.bitbucket.rozd.airscaffold
{
import com.github.airext.DeviceInfo;
import com.testfairy.AirTestFairy;

import feathers.controls.Alert;

import feathers.controls.Button;
import feathers.controls.Label;
import feathers.controls.Screen;
import feathers.layout.AnchorLayout;
import feathers.layout.AnchorLayoutData;

import starling.events.Event;

public class App extends Screen
{
    //--------------------------------------------------------------------------
    //
    //  Constructor
    //
    //--------------------------------------------------------------------------

    public function App()
    {
        super();

        this.layout = new AnchorLayout();

        AirTestFairy.begin("19e391f89be046838f04cca47f7b51aeacff4e72");
    }

    //--------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //--------------------------------------------------------------------------

    override protected function initialize():void
    {
        super.initialize();

        var label:Label = new Label();
        label.layoutData = new AnchorLayoutData();
        AnchorLayoutData(label.layoutData).verticalCenter = 0;
        AnchorLayoutData(label.layoutData).horizontalCenter = 0;
        label.text = "Hello World!";
        addChild(label);

        var button:Button = new Button();
        button.label = "Show Model";
        button.addEventListener(Event.TRIGGERED, function (event:Event):void
        {
            Alert.show(DeviceInfo.sharedInstance().general.model);
        })
    }
}
}
